<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/worker.php';

$firstPerson = new Worker();
$firstPerson->name = "John";
$firstPerson->age = 25;
$firstPerson->salary = 1000;

$secondPerson = new Worker();
$secondPerson->name = "Вася";
$secondPerson->age = 26;
$secondPerson->salary = 2000;

echo $firstPerson->salary + $secondPerson->salary;
echo '</br>';
$ageSum = $firstPerson->age + $secondPerson->age;
echo $ageSum;
